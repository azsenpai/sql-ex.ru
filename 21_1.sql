-- ������� ������������ ���� ��, ����������� ������ ��������������, � �������� ���� ������ � ������� PC.
-- �������: maker, ������������ ����. 

SELECT maker, MAX(price) AS max_price
FROM (
  SELECT DISTINCT maker, price
  FROM Product AS T1, PC AS T2
  WHERE T1.type = 'PC' AND T1.model = T2.model
) AS T
GROUP BY maker
