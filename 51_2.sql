--  Найдите названия кораблей, имеющих наибольшее число орудий среди всех имеющихся кораблей такого же водоизмещения (учесть корабли из таблицы Outcomes).

SELECT T1.name
FROM (SELECT T1.name, T2.displacement, T2.numGuns
FROM Ships T1
LEFT JOIN Classes T2
ON T1.class = T2.class
WHERE T2.displacement IS NOT NULL AND T2.numGuns IS NOT NULL
UNION
SELECT T1.ship name, T2.displacement, T2.numGuns
FROM Outcomes T1
LEFT JOIN Classes T2
ON T1.ship = T2.class
WHERE T1.ship NOT IN (SELECT name FROM Ships) AND T2.displacement IS NOT NULL AND T2.numGuns IS NOT NULL) T1
JOIN (SELECT displacement, MAX(numGuns) max_numGuns
FROM (SELECT T1.name, T2.displacement, T2.numGuns
FROM Ships T1
LEFT JOIN Classes T2
ON T1.class = T2.class
WHERE T2.displacement IS NOT NULL AND T2.numGuns IS NOT NULL
UNION
SELECT T1.ship name, T2.displacement, T2.numGuns
FROM Outcomes T1
LEFT JOIN Classes T2
ON T1.ship = T2.class
WHERE T1.ship NOT IN (SELECT name FROM Ships) AND T2.displacement IS NOT NULL AND T2.numGuns IS NOT NULL) T
GROUP BY displacement) T2
ON T1.displacement = T2.displacement AND T1.numGuns = T2.max_numGuns
