--  Пронумеровать строки из таблицы Product в следующем порядке: имя производителя в порядке убывания числа производимых им моделей (при одинаковом числе моделей имя производителя в алфавитном порядке по возрастанию), номер модели (по возрастанию).
--  Вывод: номер в соответствии с заданным порядком, имя производителя (maker), модель (model)

SELECT ROW_NUMBER() OVER(ORDER BY T2.amount DESC) AS '#', T1.maker, T1.model
FROM Product T1 LEFT JOIN
(SELECT maker, COUNT(*) AS amount FROM Product GROUP BY maker) T2
ON T1.maker = T2.maker
ORDER BY T2.amount DESC, T1.maker, T1.model
