--  C точностью до двух десятичных знаков определить среднее количество краски на квадрате.

SELECT CAST((SELECT ISNULL(SUM(B_VOL), 0) FROM utB)*1.0 / (SELECT COUNT(*) FROM utQ) AS NUMERIC(6, 2))
