-- ������� ���� ������� PC, ������� ���������� �������� � RAM. � ���������� ������
-- ���� ����������� ������ ���� ���, �.�. (i,j), �� �� (j,i),
-- ������� ������: ������ � ������� �������, ������ � ������� �������, �������� � RAM.

SELECT DISTINCT P1.model AS model1, P2.model AS model2, P1.speed AS speed, P1.ram AS ram
FROM PC AS P1, PC AS P2
WHERE P1.model > P2.model AND P1.speed = P2.speed AND P1.ram = P2.ram
