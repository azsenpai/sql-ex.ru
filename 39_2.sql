--  Найдите корабли, "сохранившиеся для будущих сражений"; т.е. выведенные из строя в одной битве (damaged), они участвовали в другой, произошедшей позже. 

SELECT DISTINCT t1_ship
FROM (
	(SELECT ship AS t1_ship, date AS t1_date FROM Outcomes INNER JOIN Battles ON battle = name WHERE result = 'damaged') AS T1
	INNER JOIN
	(SELECT ship AS t2_ship, date AS t2_date FROM Outcomes INNER JOIN Battles ON battle = name) AS T2
	ON t1_ship = t2_ship AND t1_date < t2_date
)


SELECT *
FROM Battles

SELECT *
FROM Outcomes
