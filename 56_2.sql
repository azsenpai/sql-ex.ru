--  Для каждого класса определите число кораблей этого класса, потопленных в сражениях. Вывести: класс и число потопленных кораблей. 

SELECT T1.class, ISNULL(T2.sunks, 0) sunks
FROM Classes T1
LEFT JOIN (SELECT class, COUNT(class) sunks
FROM (SELECT ISNULL(T2.class, T1.ship) class
FROM (SELECT ship
FROM Outcomes
WHERE result = 'sunk') T1
LEFT JOIN Ships T2
ON T1.ship = T2.name) T
GROUP BY class) T2
ON T1.class = T2.class
