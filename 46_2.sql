-- Для каждого корабля, участвовавшего в сражении при Гвадалканале (Guadalcanal),
-- вывести название, водоизмещение и число орудий.

-- T1 => Outcomes
-- T2 => Ships
-- T3 => Classes

SELECT T1.ship, T3.displacement, T3.numGuns
FROM Outcomes T1 LEFT JOIN Ships T2
ON T1.ship = T2.name
LEFT JOIN Classes T3
ON ISNULL(T2.class, T1.ship) = T3.class
WHERE T1.battle = 'Guadalcanal'


SELECT T1.ship, T2.displacement, T2.numGuns
FROM
(SELECT T1.ship, ISNULL(T2.class, T1.ship) AS class
FROM Outcomes AS T1 LEFT JOIN
Ships AS T2
ON T1.ship = T2.name
WHERE T1.battle = 'Guadalcanal') T1 LEFT JOIN Classes T2
ON T1.class = T2.class
