-- �������: 30 (Serge I: 2003-02-14)
-- � �������������, ��� ������ � ������ ����� �� ������ ������ ������ ����������� ������������ ����� ��� (��������� ������ � �������� �������� ������� code), ��������� �������� �������, � ������� ������� ������ �� ������ ���� ���������� �������� ����� ��������������� ���� ������.
-- �����: point, date, ��������� ������ ������ �� ���� (out), ��������� ������ ������ �� ���� (inc).
-- ������������� �������� ������� ��������������� (NULL). 

SELECT COALESCE(T1.point, T2.point) AS point, COALESCE(T1.date, T2.date) AS date, out, inc
FROM
  (
    SELECT point, date, SUM(inc) AS inc
    FROM Income
    GROUP BY point, date
  ) AS T1
  FULL JOIN
  (
    SELECT point, date, SUM(out) AS out
    FROM Outcome
    GROUP BY point, date
  ) AS T2
  ON T1.point = T2.point AND T1.date = T2.date
