-- � �������������, ��� ������ � ������ ����� �� ������ ������ ������ �����������
-- �� ���� ������ ���� � ���� [�.�. ��������� ���� (�����, ����)], �������� ������
-- � ��������� ������� (�����, ����, ������, ������).
-- ������������ ������� Income_o � Outcome_o. 

SELECT COALESCE(T1.point, T2.point) AS point, COALESCE(T1.date, T2.date) AS date, inc, out
FROM
  Income_o AS T1 FULL JOIN Outcome_o AS T2
  ON T1.point = T2.point AND T1.date = T2.date 
