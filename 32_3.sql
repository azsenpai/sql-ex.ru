-- Одной из характеристик корабля является половина куба калибра его главных орудий (mw).
-- С точностью до 2 десятичных знаков определите среднее значение mw для кораблей каждой страны, у которой есть корабли в базе данных.

SELECT country, CAST(AVG(mw) AS DECIMAL(6, 2)) AS avg_mw
FROM (
	SELECT country, bore*bore*bore / 2 AS mw
	FROM (
		(SELECT DISTINCT ship FROM Outcomes) AS O
		JOIN
		Classes
		ON ship = class AND ship NOT IN (SELECT DISTINCT name FROM Ships)
	)

	UNION ALL

	SELECT country, bore*bore*bore / 2 AS mw
	FROM (
		Classes AS C
		JOIN
		Ships AS S
		ON C.class = S.class
	)
) AS T
GROUP BY country
ORDER BY country;
