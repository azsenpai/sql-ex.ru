-- Используя таблицу Product, определить количество производителей, выпускающих по одной модели.

SELECT
	COUNT(*)
FROM
	(
		SELECT COUNT(maker) AS qty
		FROM Product
		GROUP BY maker
	)
	AS product_qty
WHERE
	qty = 1
