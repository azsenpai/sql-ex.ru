-- ������� ������ ��-���������, �������� ������� ������ �������� ������ �� ��.
-- �������: type, model, speed

SELECT DISTINCT T1.type AS type, T2.model AS model, T2.speed AS speed
FROM Product AS T1, (
  SELECT model, speed
  FROM Laptop
  WHERE speed < (
    SELECT MIN(speed) AS min_speed
    FROM PC
  )
) AS T2
WHERE T1.type = 'Laptop' AND T1.model = T2.model
