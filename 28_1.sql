-- ������� ������� ������ ����� �� (���� �������� ��� ����) ��� ��������������, ������� ��������� � ��������.
-- �������: ������� ������ HD 

SELECT AVG(hd) AS avg_hd
FROM PC
WHERE model IN (
  SELECT model
  FROM Product
  WHERE type = 'PC' AND maker IN (
    SELECT maker
    FROM Product
    WHERE type = 'Printer'
  )
)
