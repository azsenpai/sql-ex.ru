-- �� �������������� �������������� �������� �� ������ 1922 �.
-- ����������� ������� �������� ������� �������������� ����� 35 ���.����.
-- ������� �������, ���������� ���� ������� (��������� ������ ������� c ��������� ����� ������ �� ����). ������� �������� ��������. 

SELECT T2.name AS name
FROM 
  Classes AS T1 INNER JOIN (
    SELECT name, class, launched
    FROM Ships
  ) AS T2
  ON T1.class = T2.class
WHERE T1.type = 'bb' AND T1.displacement > 35000 AND T2.launched IS NOT NULL AND T2.launched >= 1922

