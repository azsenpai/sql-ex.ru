--  Найдите классы, в которые входит только один корабль из базы данных (учесть также корабли в Outcomes).

SELECT class
FROM (
	SELECT S.class AS class, S.name AS name
	FROM
		Classes AS C JOIN Ships AS S
		ON C.class = S.class

	UNION

	SELECT C.class AS class, O.ship AS name
	FROM
		Classes AS C JOIN Outcomes AS O
		ON C.class = O.ship
)
AS T
GROUP BY class
HAVING COUNT(class) = 1;


SELECT class
FROM (
	SELECT class, COUNT(class) AS qty_class
	FROM (
		SELECT S.class AS class, S.name AS name
		FROM
			Classes AS C JOIN Ships AS S
			ON C.class = S.class

		UNION

		SELECT C.class AS class, O.ship AS name
		FROM
			Classes AS C JOIN Outcomes AS O
			ON C.class = O.ship
	)
	AS T
	GROUP BY class
)
AS T
WHERE qty_class = 1;
 
--	

SELECT name
FROM Ships
WHERE class = name

UNION

SELECT ship
FROM Outcomes
WHERE (SELECT COUNT(*) FROM Ships WHERE name = class AND name = ship) = 0

SELECT T1.class, T1.qty, T2.qty
FROM
	(
		SELECT T1.class, COUNT(T1.class) AS qty
		FROM
			Classes AS T1 JOIN Ships AS T2
			ON T1.class = T2.class
		GROUP BY T1.class
	)
	AS T1

	LEFT JOIN
	
	(
		SELECT class, COUNT(class) AS qty
		FROM
			Classes JOIN Outcomes ON
			class = ship
		GROUP BY class
	)
	AS T2

	ON T1.class = T2.class

