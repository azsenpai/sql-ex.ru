SELECT DISTINCT maker
FROM Product
WHERE type = 'Printer' AND maker IN(SELECT maker FROM Product WHERE type = 'PC' AND model IN(SELECT model
FROM PC
WHERE ram = (SELECT MIN(ram) AS min_ram FROM PC) AND speed = (SELECT MAX(speed) AS max_speed FROM PC WHERE ram = (SELECT MIN(ram) AS min_ram FROM PC))))
