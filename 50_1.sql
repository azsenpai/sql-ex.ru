-- ������� ��������, � ������� ����������� ������� ������ Kongo �� ������� Ships.

SELECT DISTINCT battle
FROM
  (
    SELECT name
    FROM Ships
    WHERE class = 'Kongo'
  ) AS T1
  INNER JOIN
  (
    SELECT ship, battle
    FROM Outcomes
  ) AS T2
  ON T1.name = T2.ship
