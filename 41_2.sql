-- Для ПК с максимальным кодом из таблицы PC вывести все его характеристики (кроме кода) в два столбца:
-- - название характеристики (имя соответствующего столбца в таблице PC);
-- - значение характеристики 

SELECT 'model' AS name, CAST(model AS VARCHAR(50)) AS value
FROM PC
WHERE code = (SELECT MAX(code) AS code FROM PC)

UNION ALL

SELECT 'speed' AS name, CAST(speed AS VARCHAR(50)) AS value
FROM PC
WHERE code = (SELECT MAX(code) AS code FROM PC)

UNION ALL

SELECT 'ram' AS name, CAST(ram AS VARCHAR(50)) AS value
FROM PC
WHERE code = (SELECT MAX(code) AS code FROM PC)

UNION ALL

SELECT 'hd' AS name, CAST(hd AS VARCHAR(50)) AS value
FROM PC
WHERE code = (SELECT MAX(code) AS code FROM PC)

UNION ALL

SELECT 'cd' AS name, CAST(cd AS VARCHAR(50)) AS value
FROM PC
WHERE code = (SELECT MAX(code) AS code FROM PC)

UNION ALL

SELECT 'price' AS name, CAST(price AS VARCHAR(50)) AS value
FROM PC
WHERE code = (SELECT MAX(code) AS code FROM PC)

