-- ������� ������, ������� �����-���� ������ ������� ������ �������� ('bb') � ������� �����-���� ������ ��������� ('bc').

SELECT DISTINCT country
FROM Classes
WHERE type = 'bb'

INTERSECT

SELECT DISTINCT country
FROM Classes
WHERE type = 'bc'

-- 1 - solution

SELECT DISTINCT country
FROM Classes AS T
WHERE (
  SELECT COUNT(type) AS type_bb
  FROM Classes
  WHERE type = 'bb' AND country = T.country
) > 0 AND (
  SELECT COUNT(type) AS type_bc
  FROM Classes
  WHERE type = 'bc' AND country = T.country
) > 0
