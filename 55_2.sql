--  Для каждого класса определите год, когда был спущен на воду первый корабль этого класса. Если год спуска на воду головного корабля неизвестен, определите минимальный год спуска на воду кораблей этого класса. Вывести: класс, год. 

SELECT T1.class, T2.min_launched
FROM Classes T1
LEFT JOIN (SELECT class, MIN(launched) as min_launched FROM Ships
GROUP BY class) T2
ON T1.class = T2.class
