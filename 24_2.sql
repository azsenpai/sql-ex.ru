-- ����������� ������ ������� ����� �����, ������� ����� ������� ���� �� ���� ��������� � ���� ������ ���������.

SELECT model
FROM PC
WHERE price = (
  SELECT MAX(max_price) AS max_price
  FROM (
    SELECT MAX(price) AS max_price
    FROM PC
    UNION
    SELECT MAX(price) AS max_price
    FROM Laptop
    UNION
    SELECT MAX(price) AS max_price
    FROM Printer
  ) as T
)

UNION

SELECT model
FROM Laptop
WHERE price = (
  SELECT MAX(max_price) AS max_price
  FROM (
    SELECT MAX(price) AS max_price
    FROM PC
    UNION
    SELECT MAX(price) AS max_price
    FROM Laptop
    UNION
    SELECT MAX(price) AS max_price
    FROM Printer
  ) as T
)

UNION

SELECT model
FROM Printer
WHERE price = (
  SELECT MAX(max_price) AS max_price
  FROM (
    SELECT MAX(price) AS max_price
    FROM PC
    UNION
    SELECT MAX(price) AS max_price
    FROM Laptop
    UNION
    SELECT MAX(price) AS max_price
    FROM Printer
  ) as T
)
