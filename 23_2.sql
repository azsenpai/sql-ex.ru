SELECT maker
FROM Product
WHERE type = 'PC' AND model IN (SELECT model FROM PC WHERE speed >= 750)
INTERSECT
SELECT maker
FROM Product
WHERE type = 'Laptop' AND model IN (SELECT model FROM Laptop WHERE speed >= 750)
