-- ������� �������������� ����� ������� ������� ���������.
-- �������: maker, price

SELECT DISTINCT T1.maker AS maker, T2.price AS price
FROM Product AS T1, Printer AS T2
WHERE T1.type = 'Printer' AND T1.model = T2.model AND T2.color = 'y' AND T2.price = (
  SELECT MIN(price) AS min_price
  FROM Printer
  WHERE color = 'y'
)
