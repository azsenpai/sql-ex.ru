SELECT DISTINCT P.maker AS maker, AVG(T.hd) AS avg_hd
FROM Product AS P, (
  SELECT model, hd
  FROM PC
  WHERE model IN (
    SELECT model
    FROM Product
    WHERE type = 'PC' AND maker IN (
      SELECT maker
      FROM Product
      WHERE type = 'Printer'
    )
  )
) AS T
WHERE P.model = T.model
GROUP BY P.maker
