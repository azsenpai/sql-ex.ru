-- Найти производителей, которые выпускают более одной модели, при этом все выпускаемые производителем модели являются продуктами одного типа.
-- Вывести: maker, type

SELECT DISTINCT maker, type
FROM Product AS P
WHERE (
  SELECT COUNT(maker) AS count_maker
  FROM Product
  WHERE maker = P.maker
  GROUP BY maker
) > 1 AND (
  SELECT COUNT(DISTINCT type) AS count_type
  FROM Product
  WHERE maker = P.maker
) = 1
