--  С точностью до 2-х десятичных знаков определите среднее число орудий всех линейных кораблей (учесть корабли из таблицы Outcomes). 

SELECT CAST(AVG(numGuns * 1.0) AS NUMERIC(6, 2)) AS avg_numGuns
FROM (SELECT T1.name, T2.numGuns
FROM Ships T1
INNER JOIN Classes T2
ON T1.class = T2.class
WHERE T2.type = 'bb'
UNION ALL
SELECT DISTINCT T1.ship AS name, T2.numGuns
FROM Outcomes T1
INNER JOIN Classes T2
ON T1.ship = T2.class
WHERE T2.type = 'bb' AND T1.ship NOT IN (SELECT name FROM Ships)) T
