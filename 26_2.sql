-- ������� ������� ���� �� � ��-���������, ���������� �������������� A (��������� �����).
-- �������: ���� ����� ������� ����.

SELECT AVG(price) AS avg_price
FROM (
  SELECT price
  FROM PC
  WHERE model IN (
    SELECT model
    FROM Product
    WHERE maker = 'A' AND type = 'PC'
  )
  
  UNION ALL

  SELECT price
  FROM Laptop
  WHERE model IN (
    SELECT model
    FROM Product
    WHERE maker = 'A' AND type = 'Laptop'
  )
) AS T
