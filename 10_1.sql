SELECT model, price
FROM Printer
WHERE price = (
  SELECT MAX(price) AS max_price
  FROM Printer
)
