--  Посчитать остаток денежных средств на каждом пункте приема для базы данных с отчетностью не чаще одного раза в день. Вывод: пункт, остаток. 

SELECT T1.point, ISNULL(T1.inc, 0) - ISNULL(T2.out, 0) rest
FROM (SELECT point, SUM(inc) inc FROM Income_o
GROUP BY point) T1
LEFT JOIN (SELECT point, SUM(out) out FROM Outcome_o
GROUP BY point) T2
ON T1.point = T2.point
