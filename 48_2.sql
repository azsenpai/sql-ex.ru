--  Найдите классы кораблей, в которых хотя бы один корабль был потоплен в сражении.

SELECT DISTINCT class
FROM Outcomes T1 JOIN Ships T2
ON T1.ship = T2.name
WHERE T1.result = 'sunk'
UNION
SELECT DISTINCT ship AS class
FROM Outcomes
WHERE result = 'sunk' AND ship NOT IN (SELECT name FROM Ships) AND ship IN (SELECT class FROM Classes)
