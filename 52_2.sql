--  Определить названия всех кораблей из таблицы Ships, которые могут быть линейным японским кораблем,
--  имеющим число главных орудий не менее девяти, калибр орудий менее 19 дюймов и водоизмещение не более 65 тыс.тонн 

SELECT T1.name
FROM Ships T1
INNER JOIN Classes T2
ON T1.class = T2.class
WHERE T2.country = 'Japan' AND T2.type = 'bb' AND ISNULL(T2.numGuns, 9) >= 9 AND ISNULL(T2.bore, 18) < 19 AND ISNULL(T2.displacement, 65000) <= 65000

