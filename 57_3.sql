--  Для классов, имеющих потери в виде потопленных кораблей и не менее 3 кораблей в базе данных, вывести имя класса и число потопленных кораблей. 

//!!!
SELECT T1.class
FROM Classes T1
LEFT JOIN (SELECT class, SUM(amount) amount
FROM (SELECT class, COUNT(class) amount
FROM Ships
GROUP BY class
UNION ALL
SELECT ship class, COUNT(ship) amount
FROM Outcomes
WHERE ship NOT IN (SELECT name FROM Ships)
GROUP BY ship) T
GROUP BY class) T2
ON T1.class = T2.class
WHERE ISNULL(T2.amount, 0) >= 3
//!!!



SELECT class, COUNT(class) sunks
FROM (SELECT ISNULL(T2.class, T1.ship) class
FROM Outcomes T1
LEFT JOIN Ships T2
ON T1.ship = T2.name
WHERE T1.result = 'sunk') T
GROUP BY class
HAVING class IN (SELECT T1.class
FROM Classes T1
LEFT JOIN (SELECT class, SUM(amount) amount
FROM (SELECT class, COUNT(class) amount
FROM Ships
GROUP BY class
UNION ALL
SELECT ship class, COUNT(ship) amount
FROM Outcomes
WHERE ship NOT IN (SELECT name FROM Ships)
GROUP BY ship) T
GROUP BY class) T2
ON T1.class = T2.class
WHERE ISNULL(T2.amount, 0) >= 3)
